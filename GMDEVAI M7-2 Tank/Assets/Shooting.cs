using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public GameObject bullet;
    public GameObject turret;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    void Fire()
    {
        
            GameObject s = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
            s.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
        
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Fire();
        }
    }
}
