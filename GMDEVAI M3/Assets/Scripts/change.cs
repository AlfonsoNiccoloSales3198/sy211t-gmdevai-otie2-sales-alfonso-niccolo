using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class change : MonoBehaviour
{
    public GameObject Hope;
    public GameObject Ichi;
    public GameObject Mita;

    public void HActi()
    {
        Ichi.GetComponent<FollowPath>().enabled = false;
        Mita.GetComponent<FollowPath>().enabled = false;
        Hope.GetComponent<FollowPath>().enabled = true;
    }

    public void IActi()
    {
        Mita.GetComponent<FollowPath>().enabled = false;
        Hope.GetComponent<FollowPath>().enabled = false;
        Ichi.GetComponent<FollowPath>().enabled = true;
    }

    public void MActi()
    {
        Hope.GetComponent<FollowPath>().enabled = false;
        Ichi.GetComponent<FollowPath>().enabled = false;
        Mita.GetComponent<FollowPath>().enabled = true;
    }
}
