using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TravelForwardToGoal : MonoBehaviour
{
    public Transform goal;
    float speed = 4;
    float rotSpeed = 4;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 lookatGoal = new Vector3(goal.position.x, this.transform.position.y, goal.position.z);



        Vector3 direction = lookatGoal - transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotSpeed);

        if (Vector3.Distance(lookatGoal, transform.position) > 4)
        {
            transform.Translate(0, 0, speed * Time.deltaTime);
        }
    }
}
